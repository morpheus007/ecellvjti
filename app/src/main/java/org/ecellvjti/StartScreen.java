package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StartScreen extends Activity {
    long checks, checkf = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);
        Thread timer = new Thread() {
            public void run() {
                try {
                    checks = System.currentTimeMillis();
                    sleep(3000);
                } catch (InterruptedException e) {
                    finish();
                } finally {
                    if (checkf < checks) {
                        Intent openMainActivity = new Intent(
                                "org.ecellvjti.MAINACTIVITY");
                        startActivity(openMainActivity);
                    }
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        checkf = System.currentTimeMillis();
        finish();
    }

}
