package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Events extends Activity implements View.OnClickListener {
    Button yr, mang, app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        yr = (Button) findViewById(R.id.byr);
        mang = (Button) findViewById(R.id.bmang);
        app = (Button) findViewById(R.id.bapp);
        yr.setOnClickListener(this);
        mang.setOnClickListener(this);
        app.setOnClickListener(this);
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
        switch (arg0.getId()) {
            case R.id.byr:
                Intent startbyr = new Intent("org.ecellvjti.YEAR_ROUND");
                startActivity(startbyr);
                break;
            case R.id.bmang:
                Intent startbmang = new Intent("org.ecellvjti.MANAGERIA");
                startActivity(startbmang);
                break;
            case R.id.bapp:
                Intent startapp = new Intent("org.ecellvjti.APP_PRENEUR");
                startActivity(startapp);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }
}
