package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Techno extends Activity implements View.OnClickListener {
    Button unicus, hire, wallst, web, conslt, freako, biz, scam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.techno);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        unicus = (Button) findViewById(R.id.bunicus);
        hire = (Button) findViewById(R.id.bhire);
        wallst = (Button) findViewById(R.id.bwallstreet);
        web = (Button) findViewById(R.id.bweb_preneur);
        conslt = (Button) findViewById(R.id.bconsultant);
        freako = (Button) findViewById(R.id.bfreako);
        biz = (Button) findViewById(R.id.bbiz_quiz);
        scam = (Button) findViewById(R.id.bscam);
        unicus.setOnClickListener(this);
        hire.setOnClickListener(this);
        wallst.setOnClickListener(this);
        web.setOnClickListener(this);
        conslt.setOnClickListener(this);
        freako.setOnClickListener(this);
        biz.setOnClickListener(this);
        scam.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.bunicus:
                Intent startuni = new Intent("org.ecellvjti.UNICUS");
                startActivity(startuni);
                break;
            case R.id.bhire:
                Intent starthire = new Intent("org.ecellvjti.HIRE");
                startActivity(starthire);
                break;
            case R.id.bwallstreet:
                Intent startwall = new Intent("org.ecellvjti.WALLSTREET");
                startActivity(startwall);
                break;
            case R.id.bweb_preneur:
                Intent startweb = new Intent("org.ecellvjti.WEB_PRENEUR");
                startActivity(startweb);
                break;
            case R.id.bconsultant:
                Intent startconslt = new Intent("org.ecellvjti.CONSULTANT");
                startActivity(startconslt);
                break;
            case R.id.bfreako:
                Intent startfreako = new Intent("org.ecellvjti.FREAKO");
                startActivity(startfreako);
                break;
            case R.id.bbiz_quiz:
                Intent startbiz = new Intent("org.ecellvjti.BIZ_QUIZ");
                startActivity(startbiz);
                break;
            case R.id.bscam:
                Intent startscam = new Intent("org.ecellvjti.SCAM");
                startActivity(startscam);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }
}
