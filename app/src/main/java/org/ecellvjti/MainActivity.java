package org.ecellvjti;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity implements View.OnClickListener {
    ImageButton events, contact, about, spons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        events = (ImageButton) findViewById(R.id.ibevents);
        contact = (ImageButton) findViewById(R.id.ibcontactus);
        about = (ImageButton) findViewById(R.id.ibabout);
        spons = (ImageButton) findViewById(R.id.ibspons);
        events.setOnClickListener(this);
        contact.setOnClickListener(this);
        about.setOnClickListener(this);
        spons.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
        case R.id.ibevents:
			Intent startEvents = new Intent("org.ecellvjti.EVENTS");
			startActivity(startEvents);
			break;
            case R.id.ibcontactus:
                Intent startContact = new Intent("org.ecellvjti.CONTACT");
                startActivity(startContact);
                break;
            case R.id.ibabout:
                Intent startAbout = new Intent("org.ecellvjti.ABOUT");
                startActivity(startAbout);
                break;
            case R.id.ibspons: {
                Intent startSpons = new Intent("org.ecellvjti.SPONSERS");
                startActivity(startSpons);
            }
        }
    }

}
