package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Manageria extends Activity implements View.OnClickListener {
    Button techno, prati;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manageria);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        techno = (Button) findViewById(R.id.btechno);
        prati = (Button) findViewById(R.id.bprati);
        techno.setOnClickListener(this);
        prati.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btechno:
                Intent starttechno = new Intent("org.ecellvjti.TECHNO");
                startActivity(starttechno);
                break;
            case R.id.bprati:
                Intent startprati = new Intent("org.ecellvjti.PRATI");
                startActivity(startprati);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }
}
