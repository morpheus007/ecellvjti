package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Prati extends Activity implements View.OnClickListener {
    Button ado, bob, business, manage_road;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prati);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        ado = (Button) findViewById(R.id.bado);
        bob = (Button) findViewById(R.id.bbob);
        business = (Button) findViewById(R.id.bbusiness);
        manage_road = (Button) findViewById(R.id.bmanage_road);
        ado.setOnClickListener(this);
        bob.setOnClickListener(this);
        business.setOnClickListener(this);
        manage_road.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.bado:
                Intent startado = new Intent("org.ecellvjti.ADOMANIA");
                startActivity(startado);
                break;
            case R.id.bbob:
                Intent startbob = new Intent("org.ecellvjti.BOB");
                startActivity(startbob);
                break;
            case R.id.bbusiness:
                Intent startbusiness = new Intent("org.ecellvjti.BUSINESS");
                startActivity(startbusiness);
                break;
            case R.id.bmanage_road:
                Intent startmanage = new Intent("org.ecellvjti.ROADIES");
                startActivity(startmanage);
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }
}