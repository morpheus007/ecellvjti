package org.ecellvjti;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Year_Round extends Activity implements View.OnClickListener {
    Button idea, brain, product, aware, ithink, mandi, pitch, mentor, trinity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.year_round);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        idea = (Button) findViewById(R.id.bidea);
        brain = (Button) findViewById(R.id.bbrain);
        product = (Button) findViewById(R.id.bproduct);
        aware = (Button) findViewById(R.id.bawareness);
        ithink = (Button) findViewById(R.id.bithink);
        mandi = (Button) findViewById(R.id.bmandi);
        pitch = (Button) findViewById(R.id.bpitch_it);
        mentor = (Button) findViewById(R.id.bmentor);
        trinity = (Button) findViewById(R.id.btrinity);
        idea.setOnClickListener(this);
        brain.setOnClickListener(this);
        product.setOnClickListener(this);
        aware.setOnClickListener(this);
        ithink.setOnClickListener(this);
        mandi.setOnClickListener(this);
        pitch.setOnClickListener(this);
        mentor.setOnClickListener(this);
        trinity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.bidea:
                Intent startidea = new Intent("org.ecellvjti.IDEA");
                startActivity(startidea);
                break;
            case R.id.bbrain:
                Intent startbrain = new Intent("org.ecellvjti.BRAIN");
                startActivity(startbrain);
                break;
            case R.id.bproduct:
                Intent startproduct = new Intent("org.ecellvjti.PRODUCT");
                startActivity(startproduct);
                break;
            case R.id.bawareness:
                Intent startaware = new Intent("org.ecellvjti.AWARE");
                startActivity(startaware);
                break;
            case R.id.bithink:
                Intent startithink = new Intent("org.ecellvjti.ITHINK");
                startActivity(startithink);
                break;
            case R.id.bmandi:
                Intent startmandi = new Intent("org.ecellvjti.MANDI");
                startActivity(startmandi);
                break;
            case R.id.bpitch_it:
                Intent startpitch = new Intent("org.ecellvjti.PITCH");
                startActivity(startpitch);
                break;
            case R.id.bmentor:
                Intent startment = new Intent("org.ecellvjti.MENTOR");
                startActivity(startment);
                break;
            case R.id.btrinity:
                Intent starttrinity = new Intent("org.ecellvjti.TRINITY");
                startActivity(starttrinity);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.developers:
                Intent develop = new Intent("org.ecellvjti.DEVELOPERS");
                startActivity(develop);
                break;
            case R.id.exit:
                finish();
        }
        return false;
    }
}
